extends Node

onready var stoplight = $Stoplight

func _ready():
	stoplight.play()

	var result = wait(5, 'green')
	$WalkButton/TextureButton.connect('pressed', result, 'resume', ['interrupted on green'], CONNECT_ONESHOT)
	yield(result, 'completed')

	result = wait(5, 'yellow')
	$WalkButton/TextureButton.connect('pressed', result, 'resume', ['interrupted on yellow'], CONNECT_ONESHOT)
	yield(result, 'completed')
	
	result = wait(5, 'red')
	$WalkButton/TextureButton.connect('pressed', result, 'resume', ['interrupted on red'], CONNECT_ONESHOT)
	yield(result, 'completed')

func wait(time, color):
	print('waiting for: ' + color)
	var result = yield(get_tree().create_timer(time), 'timeout')
	if result:
		print(result)
	stoplight.animation = color
	print('done: ' + color)

func _on_completed():
	print('completed')

func _on_WalkButton_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		print ("Walk Button not functioning.")